<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Sobre';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Site feito por Samara Matos Amaral e Sarah Juliane Oliveira Magalhães, alunas do IFNMG Campus Januária do 2°Informática A.
    </p>

    
</div>
