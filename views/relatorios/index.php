<?php
 
use yii\helpers\Html;
 
$this->title = 'Relatórios';
$this->params['breadcrumbs'][] = $this->title;
?>
 
<div class="relatorios-index">
 
   <h1><?= Html::encode($this->title) ?></h1>
   <?= Html::a('Quantidade de produtos por firma', ['relatorio1'], ['class' => 'btn btn-success']) ?><br><br>
   <?= Html::a('Quantidade de produtos por categoria', ['relatorio2'], ['class' => 'btn btn-success']) ?><br><br>
   <?= Html::a('Preço total das compras', ['relatorio3'], ['class' => 'btn btn-success']) ?><br><br>
   <?= Html::a('Quantidade de produtos por lista', ['relatorio4'], ['class' => 'btn btn-success']) ?><br><br>
   <?= Html::a('Quantidade de compras não pagas', ['relatorio5'], ['class' => 'btn btn-success']) ?>
 
</div>
