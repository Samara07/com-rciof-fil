<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SsLista */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ss-lista-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'DATA')->textInput([ 'type' => 'date' ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
