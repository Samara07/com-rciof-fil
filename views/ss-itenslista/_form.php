<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\SsProduto;
use app\models\SsLista;

/* @var $this yii\web\View */
/* @var $model app\models\SsItenslista */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ss-itenslista-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'PRODUTO_ID')->
       dropDownList(ArrayHelper::map(SsProduto::find()
           ->orderBy('DESCRICAO')
           ->all(),'ID','DESCRICAO'),
           ['prompt' => 'Selecione um produto'] )
    ?>

<?= $form->field($model, 'LISTA_ID')->
       dropDownList(ArrayHelper::map(SsLista::find()
           ->orderBy('ID')
           ->all(),'ID','ID'),
           ['prompt' => 'Selecione o código de uma lista'] )
    ?>

<?= $form->field($model,'STATUS')->dropDownList([
'0'=>'Pago',
'1'=>'Não pago'
],
['prompt' => 'Selecione uma situação'] );
 ?>

    <?= $form->field($model, 'QTD')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
