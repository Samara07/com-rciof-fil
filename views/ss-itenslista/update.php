<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SsItenslista */

$this->title = 'Adicionar Itens na Lista: ' . $model->PRODUTO_ID;
$this->params['breadcrumbs'][] = ['label' => 'Itens da Lista', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->PRODUTO_ID, 'url' => ['view', 'PRODUTO_ID' => $model->PRODUTO_ID, 'LISTA_ID' => $model->LISTA_ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ss-itenslista-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
