<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SsItenslista */

$this->title = 'Adicionar Itens na Lista';
$this->params['breadcrumbs'][] = ['label' => 'Itens da Listas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ss-itenslista-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
