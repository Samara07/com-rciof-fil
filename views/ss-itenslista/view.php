<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SsItenslista */

$this->title = $model->PRODUTO_ID;
$this->params['breadcrumbs'][] = ['label' => 'Itens da Listas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ss-itenslista-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Atualizar', ['update', 'PRODUTO_ID' => $model->PRODUTO_ID, 'LISTA_ID' => $model->LISTA_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Deletar', ['delete', 'PRODUTO_ID' => $model->PRODUTO_ID, 'LISTA_ID' => $model->LISTA_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Tem certeza que deseja deletar esse item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'PRODUTO_ID',
            'LISTA_ID',
            'STATUS',
            'QTD',
        ],
    ]) ?>

</div>
