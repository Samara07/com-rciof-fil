<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SsItenslistaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ss-itenslista-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'PRODUTO_ID') ?>

    <?= $form->field($model, 'LISTA_ID') ?>

    <?= $form->field($model, 'STATUS') ?>

    <?= $form->field($model, 'QTD') ?>

    <div class="form-group">
        <?= Html::submitButton('Procurar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Voltar', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
