<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SsItenslistaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Itens da Listas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ss-itenslista-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Adicionar Itens na Lista', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute'=>'pRODUTO.DESCRICAO','label'=>'Produto'],
            'LISTA_ID',
            ['label' => 'Status',
                'value' => function ($model){
                if ($model->STATUS==0){
                    return 'Pago';
                }
                else{
                    return 'Não pago';
                }
            }
            ],
          //  'STATUS',
            'QTD',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
