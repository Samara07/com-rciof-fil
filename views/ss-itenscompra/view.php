<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SsItenscompra */

$this->title = $model->PRODUTO_ID;
$this->params['breadcrumbs'][] = ['label' => 'Itens da compra', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ss-itenscompra-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Atualizar', ['update', 'PRODUTO_ID' => $model->PRODUTO_ID, 'COMPRA_ID' => $model->COMPRA_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Deletar', ['delete', 'PRODUTO_ID' => $model->PRODUTO_ID, 'COMPRA_ID' => $model->COMPRA_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Tem certeza que deseja deletar esse item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'PRODUTO_ID',
            'COMPRA_ID',
            'QTD',
        ],
    ]) ?>

</div>
