<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SsItenscompra */

$this->title = 'Adicionar novo item';
$this->params['breadcrumbs'][] = ['label' => 'Ss Itenscompras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ss-itenscompra-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
