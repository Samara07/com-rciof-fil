<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SsItenscompra */

$this->title = 'Atualizar Itens da compra: ' . $model->PRODUTO_ID;
$this->params['breadcrumbs'][] = ['label' => 'Ss Itenscompras', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->PRODUTO_ID, 'url' => ['view', 'PRODUTO_ID' => $model->PRODUTO_ID, 'COMPRA_ID' => $model->COMPRA_ID]];
$this->params['breadcrumbs'][] = 'Atualizar';
?>
<div class="ss-itenscompra-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
