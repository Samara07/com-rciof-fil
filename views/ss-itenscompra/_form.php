<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\SsProduto;
use app\models\SsCompra;


/* @var $this yii\web\View */
/* @var $model app\models\SsItenscompra */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ss-itenscompra-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'PRODUTO_ID')->
       dropDownList(ArrayHelper::map(SsProduto::find()
           ->orderBy('DESCRICAO')
           ->all(),'ID','DESCRICAO'),
           ['prompt' => 'Selecione um produto'] )
    ?>


<?= $form->field($model, 'COMPRA_ID')->
       dropDownList(ArrayHelper::map(SsCompra::find()
           ->orderBy('ID')
           ->all(),'ID','DATA'),
           ['prompt' => 'Selecione uma compra'] )
    ?>

    <?= $form->field($model, 'QTD')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
