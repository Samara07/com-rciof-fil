<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\SsFirma;
use app\models\SsCategoria;

/* @var $this yii\web\View */
/* @var $model app\models\SsProduto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ss-produto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'DESCRICAO')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FIRMA_ID')->
       dropDownList(ArrayHelper::map(SsFirma::find()
           ->orderBy('ID')
           ->all(),'ID','NOME'),
           ['prompt' => 'Selecione o nome da firma'] )
    ?>

<?= $form->field($model, 'CATEGORIA_ID')->
       dropDownList(ArrayHelper::map(SsCategoria::find()
           ->orderBy('ID')
           ->all(),'ID','NOME'),
           ['prompt' => 'Selecione uma categoria'] )
    ?>
    
    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
