<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SsCompra */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ss-compra-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'DATA')->textInput([ 'type' => 'date' ]) ?>

    <?= $form->field($model,'STATUS')->dropDownList([
'0'=>'Pago',
'1'=>'Não pago'
],
['prompt' => 'Selecione uma situação'] );
 ?>


    <?= $form->field($model, 'PRECO')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
