<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SsCompra */

$this->title = 'Compra: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Ss Compras', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Enviar';
?>
<div class="ss-compra-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
