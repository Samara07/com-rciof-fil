<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SsCompraSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Compras';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ss-compra-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Adicionar Compra', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            [
                'attribute'=>'DATA',
                'format' => 'date',
                'filterInputOptions' => [
                    'type'=>'date',
                ],
            ],

            ['label' => 'STATUS',
                'value' => function ($model){
                if ($model->STATUS==0){
                    return 'Pago';
                }
                else{
                    return 'Não pago';
                }
            }
            ],
            //'STATUS',
            'PRECO',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
