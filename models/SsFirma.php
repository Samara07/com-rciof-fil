<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ss_firma".
 *
 * @property int $ID
 * @property string $NOME
 * @property int $TEL
 *
 * @property SsProduto[] $ssProdutos
 */
class SsFirma extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ss_firma';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['TEL'], 'integer'],
            [['NOME'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'Código',
            'NOME' => 'Nome',
            'TEL' => 'Telefone',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSsProdutos()
    {
        return $this->hasMany(SsProduto::className(), ['FIRMA_ID' => 'ID']);
    }
}
