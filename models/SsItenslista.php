<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ss_itenslista".
 *
 * @property int $PRODUTO_ID
 * @property int $LISTA_ID
 * @property int $STATUS
 * @property string $QTD
 *
 * @property SsProduto $pRODUTO
 * @property SsLista $lISTA
 */
class SsItenslista extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ss_itenslista';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PRODUTO_ID', 'LISTA_ID'], 'required'],
            [['PRODUTO_ID', 'LISTA_ID', 'STATUS'], 'integer'],
            [['QTD'], 'string', 'max' => 10],
            [['PRODUTO_ID', 'LISTA_ID'], 'unique', 'targetAttribute' => ['PRODUTO_ID', 'LISTA_ID']],
            [['PRODUTO_ID'], 'exist', 'skipOnError' => true, 'targetClass' => SsProduto::className(), 'targetAttribute' => ['PRODUTO_ID' => 'ID']],
            [['LISTA_ID'], 'exist', 'skipOnError' => true, 'targetClass' => SsLista::className(), 'targetAttribute' => ['LISTA_ID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PRODUTO_ID' => 'Produto',
            'LISTA_ID' => 'Lista',
            'STATUS' => 'Status',
            'QTD' => 'Quantidade',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPRODUTO()
    {
        return $this->hasOne(SsProduto::className(), ['ID' => 'PRODUTO_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLISTA()
    {
        return $this->hasOne(SsLista::className(), ['ID' => 'LISTA_ID']);
    }
}
