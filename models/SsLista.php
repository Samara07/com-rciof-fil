<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ss_lista".
 *
 * @property int $ID
 * @property string $DATA
 *
 * @property SsItenslista[] $ssItenslistas
 * @property SsProduto[] $pRODUTOs
 */
class SsLista extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ss_lista';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DATA'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'Código',
            'DATA' => 'Data',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSsItenslistas()
    {
        return $this->hasMany(SsItenslista::className(), ['LISTA_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPRODUTOs()
    {
        return $this->hasMany(SsProduto::className(), ['ID' => 'PRODUTO_ID'])->viaTable('ss_itenslista', ['LISTA_ID' => 'ID']);
    }
}
