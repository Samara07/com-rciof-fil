<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ss_produto".
 *
 * @property int $ID
 * @property string $DESCRICAO
 * @property int $FIRMA_ID
 * @property int $CATEGORIA_ID
 *
 * @property SsItenscompra[] $ssItenscompras
 * @property SsCompra[] $COMPRAs
 * @property SsItenslista[] $ssItenslistas
 * @property SsLista[] $lISTAs
 * @property SsFirma $fIRMA
 * @property SsCategoria $cATEGORIA
 */
class SsProduto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ss_produto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['FIRMA_ID', 'CATEGORIA_ID'], 'integer'],
            [['DESCRICAO'], 'string', 'max' => 20],
            [['FIRMA_ID'], 'exist', 'skipOnError' => true, 'targetClass' => SsFirma::className(), 'targetAttribute' => ['FIRMA_ID' => 'ID']],
            [['CATEGORIA_ID'], 'exist', 'skipOnError' => true, 'targetClass' => SsCategoria::className(), 'targetAttribute' => ['CATEGORIA_ID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'Código',
            'DESCRICAO' => 'Descrição',
            'FIRMA_ID' => 'Firma',
            'CATEGORIA_ID' => 'Categoria',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSsItenscompras()
    {
        return $this->hasMany(SsItenscompra::className(), ['PRODUTO_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCOMPRAs()
    {
        return $this->hasMany(SsCompra::className(), ['ID' => 'COMPRA_ID'])->viaTable('ss_itenscompra', ['PRODUTO_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSsItenslistas()
    {
        return $this->hasMany(SsItenslista::className(), ['PRODUTO_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLISTAs()
    {
        return $this->hasMany(SsLista::className(), ['ID' => 'LISTA_ID'])->viaTable('ss_itenslista', ['PRODUTO_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirma()
    {
        return $this->hasOne(SsFirma::className(), ['ID' => 'FIRMA_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoria()
    {
        return $this->hasOne(SsCategoria::className(), ['ID' => 'CATEGORIA_ID']);
    }
}
