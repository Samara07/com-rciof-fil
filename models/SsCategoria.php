<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ss_categoria".
 *
 * @property int $ID
 * @property string $NOME
 *
 * @property SsProduto[] $ssProdutos
 */
class SsCategoria extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ss_categoria';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NOME'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'Código',
            'NOME' => 'Nome',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSsProdutos()
    {
        return $this->hasMany(SsProduto::className(), ['CATEGORIA_ID' => 'ID']);
    }
}
