<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SsItenscompra;

/**
 * SsItenscompraSearch represents the model behind the search form of `app\models\SsItenscompra`.
 */
class SsItenscompraSearch extends SsItenscompra
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['COMPRA_ID'], 'integer'],
            [['QTD','pRODUTO.DESCRICAO'], 'safe'],
        ];
    }
    public function attributes()
   {
   return array_merge(parent::attributes(), ['pRODUTO.DESCRICAO']);
   }



    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SsItenscompra::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith(['pRODUTO']);
        $dataProvider->sort->attributes['pRODUTO.DESCRICAO']=[
            'asc'=>['pRODUTO.DESCRICAO'=>SORT_ASC],
            'desc'=>['pRODUTO.DESCRICAO'=>SORT_DESC],
        ];
     


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'PRODUTO_ID' => $this->PRODUTO_ID,
            'COMPRA_ID' => $this->COMPRA_ID,
        ]);

        $query->andFilterWhere(['like', 'QTD', $this->QTD]);
        $query->andFilterWhere(['LIKE', 'pRODUTO.DESCRICAO',$this->getAttribute('Ss_Produto.DESCRICAO')]);

        return $dataProvider;
    }
}
