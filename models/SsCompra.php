<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ss_compra".
 *
 * @property int $ID
 * @property string $DATA
 * @property int $STATUS
 * @property string $PRECO
 *
 * @property SsItenscompra[] $ssItenscompras
 * @property SsProduto[] $PRODUTOs
 */
class SsCompra extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ss_compra';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DATA'], 'safe'],
            [['STATUS'], 'integer'],
            [['PRECO'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'Código',
            'DATA' => 'Data',
            'STATUS' => 'Status',
            'PRECO' => 'Preço',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSsItenscompras()
    {
        return $this->hasMany(SsItenscompra::className(), ['COMPRA_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPRODUTO()
    {
        return $this->hasMany(SsProduto::className(), ['ID' => 'PRODUTO_ID'])->viaTable('ss_itenscompra', ['COMPRA_ID' => 'ID']);
    }
}
