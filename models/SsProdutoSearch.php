<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SsProduto;

/**
 * SsProdutoSearch represents the model behind the search form of `app\models\SsProduto`.
 */
class SsProdutoSearch extends SsProduto
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID'], 'integer'],
            [['DESCRICAO','firma.NOME', 'categoria.NOME'], 'safe'],
        ];
    }

    public function attributes()
   {
   return array_merge(parent::attributes(), ['firma.NOME','categoria.NOME']);
   }


    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SsProduto::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith(['firma']);
        $dataProvider->sort->attributes['firma.NOME']=[
            'asc'=>['firma.NOME'=>SORT_ASC],
            'desc'=>['firma.NOME'=>SORT_DESC],
        ];
     
        $query->joinWith(['categoria']);
        $dataProvider->sort->attributes['categoria.NOME']=[
            'asc'=>['categoria.NOME'=>SORT_ASC],
            'desc'=>['categoria.NOME'=>SORT_DESC],
        ];


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'firma_ID' => $this->FIRMA_ID,
            'CATEGORIA_ID' => $this->CATEGORIA_ID,
        ]);

        $query->andFilterWhere(['like', 'DESCRICAO', $this->DESCRICAO]);

        $query->andFilterWhere(['LIKE', 'firma.NOME',$this->getAttribute('firma.NOME')]);
        $query->andFilterWhere(['LIKE', 'categoria.NOME',$this->getAttribute('categoria.NOME')]);


        return $dataProvider;
    }
}
