<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ss_itenscompra".
 *
 * @property int $PRODUTO_ID
 * @property int $COMPRA_ID
 * @property string $QTD
 *
 * @property SsProduto $pRODUTO
 * @property SsCompra $cOMPRA
 */
class SsItenscompra extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ss_itenscompra';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PRODUTO_ID', 'COMPRA_ID'], 'required'],
            [['PRODUTO_ID', 'COMPRA_ID'], 'integer'],
            [['QTD'], 'string', 'max' => 10],
            [['PRODUTO_ID', 'COMPRA_ID'], 'unique', 'targetAttribute' => ['PRODUTO_ID', 'COMPRA_ID']],
            [['PRODUTO_ID'], 'exist', 'skipOnError' => true, 'targetClass' => SsProduto::className(), 'targetAttribute' => ['PRODUTO_ID' => 'ID']],
            [['COMPRA_ID'], 'exist', 'skipOnError' => true, 'targetClass' => SsCompra::className(), 'targetAttribute' => ['COMPRA_ID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PRODUTO_ID' => 'Produto',
            'COMPRA_ID' => 'Código da compra',
            'QTD' => 'Quantidade',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPRODUTO()
    {
        return $this->hasOne(SsProduto::className(), ['ID' => 'PRODUTO_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCOMPRA()
    {
        return $this->hasOne(SsCompra::className(), ['ID' => 'COMPRA_ID']);
    }
}
