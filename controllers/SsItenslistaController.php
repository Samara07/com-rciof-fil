<?php

namespace app\controllers;

use Yii;
use app\models\SsItenslista;
use app\models\SsItenslistaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SsItenslistaController implements the CRUD actions for SsItenslista model.
 */
class SsItenslistaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SsItenslista models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SsItenslistaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SsItenslista model.
     * @param integer $PRODUTO_ID
     * @param integer $LISTA_ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($PRODUTO_ID, $LISTA_ID)
    {
        return $this->render('view', [
            'model' => $this->findModel($PRODUTO_ID, $LISTA_ID),
        ]);
    }

    /**
     * Creates a new SsItenslista model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SsItenslista();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'PRODUTO_ID' => $model->PRODUTO_ID, 'LISTA_ID' => $model->LISTA_ID]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SsItenslista model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $PRODUTO_ID
     * @param integer $LISTA_ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($PRODUTO_ID, $LISTA_ID)
    {
        $model = $this->findModel($PRODUTO_ID, $LISTA_ID);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'PRODUTO_ID' => $model->PRODUTO_ID, 'LISTA_ID' => $model->LISTA_ID]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SsItenslista model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $PRODUTO_ID
     * @param integer $LISTA_ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($PRODUTO_ID, $LISTA_ID)
    {
        $this->findModel($PRODUTO_ID, $LISTA_ID)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SsItenslista model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $PRODUTO_ID
     * @param integer $LISTA_ID
     * @return SsItenslista the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($PRODUTO_ID, $LISTA_ID)
    {
        if (($model = SsItenslista::findOne(['PRODUTO_ID' => $PRODUTO_ID, 'LISTA_ID' => $LISTA_ID])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
