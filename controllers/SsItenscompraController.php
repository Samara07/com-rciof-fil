<?php

namespace app\controllers;

use Yii;
use app\models\SsItenscompra;
use app\models\SsItenscompraSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SsItenscompraController implements the CRUD actions for SsItenscompra model.
 */
class SsItenscompraController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SsItenscompra models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SsItenscompraSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SsItenscompra model.
     * @param integer $PRODUTO_ID
     * @param integer $COMPRA_ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($PRODUTO_ID, $COMPRA_ID)
    {
        return $this->render('view', [
            'model' => $this->findModel($PRODUTO_ID, $COMPRA_ID),
        ]);
    }

    /**
     * Creates a new SsItenscompra model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SsItenscompra();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'PRODUTO_ID' => $model->PRODUTO_ID, 'COMPRA_ID' => $model->COMPRA_ID]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SsItenscompra model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $PRODUTO_ID
     * @param integer $COMPRA_ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($PRODUTO_ID, $COMPRA_ID)
    {
        $model = $this->findModel($PRODUTO_ID, $COMPRA_ID);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'PRODUTO_ID' => $model->PRODUTO_ID, 'COMPRA_ID' => $model->COMPRA_ID]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SsItenscompra model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $PRODUTO_ID
     * @param integer $COMPRA_ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($PRODUTO_ID, $COMPRA_ID)
    {
        $this->findModel($PRODUTO_ID, $COMPRA_ID)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SsItenscompra model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $PRODUTO_ID
     * @param integer $COMPRA_ID
     * @return SsItenscompra the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($PRODUTO_ID, $COMPRA_ID)
    {
        if (($model = SsItenscompra::findOne(['PRODUTO_ID' => $PRODUTO_ID, 'COMPRA_ID' => $COMPRA_ID])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
