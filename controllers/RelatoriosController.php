<?php
 
namespace app\controllers;
use Yii;
use yii\data\SqlDataProvider;


 
class RelatoriosController extends \yii\web\Controller
{
   public function actionIndex()
   {
       return $this->render('index');
   }

   public function actionRelatorio1()
   {
       $relatorio1 = new SqlDataProvider([
        'sql' => 'SELECT ss_FIRMA.NOME, COUNT(ss_PRODUTO.ID)AS qnt FROM ss_FIRMA JOIN ss_PRODUTO ON ss_FIRMA.id = ss_PRODUTO.FIRMA_ID
        GROUP BY ss_FIRMA.NOME
        ORDER BY COUNT(ss_PRODUTO.id) DESC',
            ]
        );
        
        return $this->render('relatorio1', ['resultado' => $relatorio1]);
   }



   public function actionRelatorio3()
   {
       $relatorio3 = new SqlDataProvider([
        'sql' => 'SELECT SUM(ss_COMPRA.PRECO)AS PREÇO_TOTAL FROM ss_COMPRA',
            ]
        );
        
        return $this->render('relatorio3', ['resultado' => $relatorio3]);
   }

 



   public function actionRelatorio2()
   {
       $relatorio2 = new SqlDataProvider([
        'sql' => 'SELECT ss_CATEGORIA.NOME, COUNT(ss_PRODUTO.ID) AS Quantidade
        FROM ss_CATEGORIA JOIN ss_PRODUTO ON ss_CATEGORIA.ID = ss_PRODUTO.FIRMA_ID
        GROUP BY ss_CATEGORIA.ID
        ORDER BY COUNT(ss_PRODUTO.ID) DESC',
            ]
        );
        
        return $this->render('relatorio2', ['resultado' => $relatorio2]);
   }

   public function actionRelatorio4()
   {
       $relatorio4 = new SqlDataProvider([
        'sql' => 'SELECT ss_ITENSLISTA.LISTA_ID AS Lista, COUNT(ss_ITENSLISTA.PRODUTO_ID) AS Quantidade
        FROM ss_ITENSLISTA 
        GROUP BY ss_ITENSLISTA.LISTA_ID',
            ]
        );
        
        return $this->render('relatorio4', ['resultado' => $relatorio4]);
   }

   
   public function actionRelatorio5()
   {
       $relatorio5 = new SqlDataProvider([
        'sql' => 'SELECT COUNT(ss_ITENSLISTA.status) AS Não_pago 
        FROM ss_ITENSLISTA
		WHERE ss_ITENSLISTA.status=1',
            ]
        );
        
        return $this->render('relatorio5', ['resultado' => $relatorio5]);
   }

 
}
